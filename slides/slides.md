---
marp: true
backgroundColor: #FFEFC0
style: |
  section {
    justify-content: start;
  }

  img {
    display: block;
    margin: 20px auto;
  }

  h5 {
    text-align: center
  }

  section.big h1 {
    text-align: center;
    font-size: 4em;
  }

---

# Atelier Dev
## Maven

![bg left:40% 80%](https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Apache_Maven_logo.svg/1280px-Apache_Maven_logo.svg.png)

---
##### Random développeur après deux `mvn clean install`

![w:650](./img/meme.png)


---
##### Random développeur après la configuration d'un plugin dans le `pom.xml`

![w:650](./img/meme2.jpg)

---
<!-- _class: big -->
# Maven, c'est quoi ?

---
## Maven, c'est quoi ?

- Outil de build

- Facilite et automatise des tâches de projets : compilation, tests unitaires, déploiement...

- Gestion des dépendances

- Principalement utilisé en Java, avec Gradle

- Concepts :
  - *Artifacts*
  - *Lifecycles* et *phases*
  - *Repositories*

---
## Plugin, dependencies ?
- Plugins :

  - Exécution de tâches pendant un build Maven (compilation, execution de tests unitaires, archivage, déploiement...)
  - Non packagés dans l'application
  - Configurés dans la partie `<build>`

---
## Plugin, dependencies ?

- Dépendances :

  - Artifacts/components requis par l'application
  - Dépendances packagées dans l'application (librairies)
  - Configurés dans la partie `<dependencies>`
  - Gestion des dépendances via le `<dependencyManagement>`

---
## Dependency scope

- Modifie le classpath pour différentes tâches de build :
- 6 dependency scopes :

  - `compile` *(Scope par défaut)* : Dépendances disponibles 
  - `provided` : Indique que le dépendance doit êre fournie par le JDK au runtime
  - `runtime` : Dépendance non requise pour la compilation mais uniquement au runtime
  - `test` : Dépendance requise uniquement pour les phases de test (compilation et exécution)
  - `system` *(deprecated)* : Similaire à provided. La configuration doit indiquer
  - `import` *(uniquement pour les dependency de type pom)*


---
## Lifecycle, phase, goal ?

![w:750](https://miro.medium.com/max/1400/1*_-K1nhOZhHeCxW1nlZ9VBw.png)

---
## Lifecycles

  - Sequence de *phases*
  - 3 lifecylces :

    - `default` : Build/déploiement du projet
    - `clean`: Nettoyage du projet
    - `site` : Generation de la documentation

---
## Phases
Chaque phase responsable d'une tâche spécifique
  - *validate* : 
  - *compile* : Compile le code source
  - *test-compile* : Compile le code source des tests
  - *test* : Lance l'exécution des tests unitaires
  - *package* : Package le code compilé vers le format configuré (`.jar`, `.war`, ...)
  - *install* : Installe le package dans un repository local
  ...


---
## Phases

- `mvn <PHASE>` déclenche toutes les phases précédentes du lifecycle de la phase indiquée

- Exemple :
  
  - `mvn install` : 
    - `install` lifecycle : *validate* + *compile* + *test-compile* + *package* + ...
 
  - `mvn clean install` : 
    - `clean` lifecycle : *pre-clean* + *clean*
    - `install` lifecycle : *validate* + *compile* + *test-compile* + *package* + ...


---
## Goals

- phase != goal
- Responsable d'une tâche précise
- Plugin : groupe de goals
- Les goals sont associés à des phases dans la configuration des plugins dans le `pom.xml`

---
## Exemple
```xml
<plugin>
  <groupId>org.codehaus.mojo</groupId>
  <artifactId>flatten-maven-plugin</artifactId>
  <configuration>
    <updatePomFile>true</updatePomFile>
  </configuration>
  <executions>
    <execution>
      <id>flatten</id>
      <phase>process-resources</phase>
      <goals>
        <goal>flatten</goal>
      </goals>
    </execution>
    <execution>
      <id>flatten.clean</id>
      <phase>clean</phase>
      <goals>
        <goal>clean</goal>
      </goals>
    </execution>
  </executions>
</plugin>
```

---
## Paramètres utiles

- `-X` : mode verbeux
- `-h` : aide
- `-o` : mode offline (ne pas tirer de nouveaux builds de dépendances présentes en local)

---
## Bonnes pratiques

- `<dependencyManagement>` et `<pluginManagement>` dans le pom parent

- Pas de `<dependency>` ni de `<plugin>` dans le pom parent, chacun des sous-modules précise les dépendances / plugins requis

- Certains plugins généraux au projet peuvent être déclarés et configurés dans le pom parent : *cyclonedx, flatten-maven-plugin*...


---
# Sources

- [Documetation Apache Maven](https://maven.apache.org/index.html)
- [Développons en Java - Maven](https://www.jmdoudoux.fr/java/dej/chap-maven.htm), JM Doudoux *(le GOAT)*
- [Baeldung, maven goals phases](https://www.baeldung.com/maven-goals-phases)

Slides réalisées avec [Marp](https://marp.app/)